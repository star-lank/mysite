from django.urls import path
from . import views

urlpatterns = [
    # http://localhost:8000/comment/
    path('update_comment', views.update_comment, name='update_comment'),
]
