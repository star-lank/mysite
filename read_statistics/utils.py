import datetime
from django.contrib.contenttypes.models import ContentType
from .models import ReadNum, ReadDetail
from django.utils import timezone
from django.db.models import Sum
from blog.models import Blog


def read_statistics_once_read(request, obj):
    ct = ContentType.objects.get_for_model(obj)
    key = '%s_%s_read' % (ct.model, obj.pk)
    if not request.COOKIES.get(key):
        # 总阅读数加1
        readnum, created = ReadNum.objects.get_or_create(content_type=ct, object_id=obj.pk)
        readnum.read_number += 1
        readnum.save()
        # 当天阅读数加1
        date = timezone.now().date()
        read_detail, created = ReadDetail.objects.get_or_create(content_type=ct, object_id=obj.pk, date=date)
        read_detail.read_number += 1
        read_detail.save()
    return key


def get_seven_days_read_data(content_type):
    today = timezone.now().date()
    dates = []
    seven_day_read_nums = []
    for i in range(7, 0, -1):
        date = today - datetime.timedelta(days=i)
        dates.append(date.strftime('%m/%d'))
        read_details = ReadDetail.objects.filter(content_type=content_type, date=date)
        result = read_details.aggregate(date_read_num_sum=Sum('read_number'))
        seven_day_read_nums.append(result['date_read_num_sum'] or 0)
    return dates, seven_day_read_nums


def get_today_hot_data():
    today = timezone.now().date()
    read_details = Blog.objects.filter(read_details__date=today) \
                       .values('id', 'title') \
                       .annotate(read_num_sum=Sum('read_details__read_number')) \
                       .order_by('-read_details__read_number')
    return read_details[:5]


def get_yesterday_hot_data():
    today = timezone.now().date()
    yesterday = today - datetime.timedelta(days=1)
    read_details = Blog.objects.filter(read_details__date=yesterday) \
                       .values('id', 'title') \
                       .annotate(read_num_sum=Sum('read_details__read_number')) \
                       .order_by('-read_details__read_number')
    return read_details[:5]
