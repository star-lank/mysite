from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db.models.fields import exceptions
from django.utils import timezone


class ReadNum(models.Model):
    read_number = models.IntegerField(default=0)

    content_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')


class ReadDetail(models.Model):
    date = models.DateField(default=timezone.now)
    read_number = models.IntegerField(default=0)

    content_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')


class ReadNumExpandMethod():
    def get_read_num(self):
        try:
            ct = ContentType.objects.get_for_model(self)  # 括号里也可填self
            readnum = ReadNum.objects.get(content_type=ct, object_id=self.id)
            return readnum.read_number
        except exceptions.ObjectDoesNotExist:
            return 0

    def get_today_read_num(self):
        try:
            ct = ContentType.objects.get_for_model(self)
            date = timezone.now().date()
            readdetail = ReadDetail.objects.get(content_type=ct, object_id=self.id, date=date)
            return readdetail.read_number
        except exceptions.ObjectDoesNotExist:
            return 0

